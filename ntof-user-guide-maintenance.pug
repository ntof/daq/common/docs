<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
// local reference to another doc
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>

// small table helpers
mixin ctd
  td(align='center')
    block

mixin cth
  th(align='center')
    block

// categories: std | bcp | info | exp | historic
rfc(category='bcp' number='2686012' consensus='false' ipr='trust200902' docName='ntof-user-guide-maintenance-01' submissionType='ntof-daq' xml:lang='en' version='3')
  front
    title(abbrev='n_ToF Maintenance Guide') n_ToF Maintenance Guide
    author(fullname='Sylvain Fargier' initials='S.F.' role='editor' surname='Fargier')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email sylvain.fargier@cern.ch
    date(year='2022' month='January')

    keyword ntof

    abstract
      t This is a Maintenance user Guide for n_ToF DAQ related applications.

  middle

    section
      name Introduction
      t
        | This document is a Maintenance Guide for n_ToF related applications,
        | it provides hints and receipes to achieve general maintenance tasks.

      // This one is a 'must' :)
      section
        name Requirements Notation
        t
          | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
          | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
          | document are to be interpreted as described in BCP 14,
          | #[xref(target='RFC2119' format='default') RFC2119].

    section
      name Project Management

      t
        | The #[eref(target='https://its.cern.ch/jira/projects/NTOFNEWDAQ/issues') NTOFNEWDAQ]
        | Jira project is used to manage n_ToF DAQ development tasks and
        | feature requests.

    section
      name Passwords and Credentials

      t
        | To access the #[xref(target="nToF_password_store") password and credentials store],
        | the #[tt ntofdev private gpg key] is #[bcp14 REQUIRED], please
        | follow the #[tt README] on this repository to install the store on your machine.
      
      t
        | This #[xref(target="nToF_password_store") password-store] is used as a
        | shared crendentials reference for n_ToF DAQ project
        | and #[bcp14 MUST] be updated anytime a password is changed.

      table(align='center')
        name n_ToF passwords
        thead
          tr
            +cth Path
            +cth Type
            +cth Purpose
            +cth Expiry
        tbody
          tr
            td #[tt Users/ntof development]
            +ctd user-account: ntofdev
            +ctd used to access machines
            +ctd 1 year
          tr
            td #[tt Users/ntof production]
            +ctd user-account: ntofprod
            +ctd not directly used, mainly maintenance
            +ctd 1 year
          tr
            td #[tt Users/ntof daq]
            +ctd user-account: ntofdaq
            +ctd used to upload data on CTA
            +ctd 1 year
          tr
            td #[tt Users/ntof root]
            +ctd user-account: root
            +ctd local-root account on daq machines
            +ctd -
          tr
            td #[tt Users/ntof ntofall]
            +ctd user-account: ntofall
            +ctd user account for the collaboration
            +ctd 1 year
          tr
            td #[tt Users/ntof ntofci]
            +ctd user-account: ntofci
            +ctd user account for GitLab-CI
            +ctd 1 year
          tr
            td #[tt Users/IPMI]
            +ctd user-account
            +ctd IPMI consoles credentials
            +ctd -
          tr
            td #[tt Users/Camera]
            +ctd user-account
            +ctd Cameras credentials
            +ctd -
          tr
            td #[tt Users/HV_Crate]
            +ctd user-account
            +ctd High-Voltage crate credentials
            +ctd -
          tr
            td #[tt Databases/DEV/DEV DB]
            +ctd oracle-account
            +ctd ntofdev account on devdb19
            +ctd 1 year
          tr
            td #[tt Databases/PROD/PROD DB]
            +ctd oracle-account
            +ctd ntof account on pdbr
            +ctd 1 year
          tr
            td #[tt Expert/sso]
            +ctd single-sign-on credentials
            +ctd expert-interface sso information
            +ctd -
          tr
            td #[tt Keys/DIMon/openshift]
            +ctd OpenShift access token
            +ctd credentials to restart web-applications from DIMon
            +ctd -
          tr
            td #[tt Keys/DIMon/ssh]
            +ctd ssh private key
            +ctd ssh key to access daq machines from DIMon
            +ctd -
          tr
            td #[tt Keys/DIMon/sso]
            +ctd single-sign-on credentials
            +ctd DIMon sso information
            +ctd -
          tr
            td #[tt Keys/ntofci]
            +ctd ssh private key
            +ctd CI deploy key (to commit on distfiles and update submodules)
            +ctd -

    section
      name Service Accounts

      t Here's a list of the Service-Accounts used in n_ToF DAQ:
      table(align='center')
        thead
          tr
            +cth Name
            +cth Description
            +cth On Update Actions
        tbody
          tr
            +ctd notfdev
            td
              ul
                li DAQ console access and sudoer
            td
              ul
                li update password-store
          tr
            +ctd ntofprod
            td
              ul
                li SSO applications owner
                li Jira project owner
            td
              ul
                li update password-store
          tr
            +ctd ntofci
            td
              ul
                li GitLab-CI account
            td
              ul
                li update password-store
                li update GitLab-CI #[tt ntof/daq] group variables
          tr
            +ctd ntofdaq
            td
              ul
                li CTA access account (RawFileMerger)
            td
              ul
                li update password-store
                li update ntofdaq.keytab on puppet-master-configuration
          tr
            +ctd ntofall
            td
              ul
                li end-user access account
            td
              ul
                li update password-store

      section
        name ntofci: Continuous Integration / Continuous Delivery

        t The #[tt ntofci] account is associated with an EOS #[tt project] : #[tt /eos/project/n/ntofci].

        t
          | The EOS share hosts #[tt distfiles] (RPMs and other artifacts), and
          | CI reports, it is exposed as a #[tt WebEOS] static site here:
          | #[eref(target='https://ntofci.web.cern.ch/')].

        t
          | #[strong Note:] the #[tt ntofci] credentials and private ssh key are installed
          | in #[tt ntof/daq] GitLab-CI group protected variables:
        ul
          li Login and password are used to publish artifacts on the EOS share (using kerberos and ssh).
          li SSH key is used to commit on repositories (packages in distfiles, or git submodules updates).

        t
          | #[strong Note:] the password #[bcp14 MUST] be updated in #[tt ntof/daq] GitLab-CI group
          | protected variables whenever renewed.

    section
      name Database accounts

      t Here's a list of #[xref(target='CERN_ORACLE') Oracle Database] Accounts used in n_ToF DAQ:
      table(align='center')
        thead
          tr
            +cth Name
            +cth Description
            +cth Database
            +cth On Update Actions
        tbody
          tr #[+ctd ntof] #[+ctd production database] #[+ctd pdbr]
            td
              ul
                li update password-store
                li re-deploy website, fixed-display and operation-control configuration
                li update EACS configuration
          tr #[+ctd ntofdev] #[+ctd development database] #[+ctd devdb19]
            td
              ul
                li update password-store

    section
      name E-Groups
      
      t Here's a list of e-groups used in n_ToF DAQ:
      table(align='center')
        thead
          tr
            +cth Name
            +cth Roles
        tbody
          tr
            +ctd ntofdaq-admins
            td
              ul
                li SSO management
                li Other e-group management
                li ntofci WebEOS management
                li ntof-daq OpenShift project management
                li DAQ Machines management
                li Control Machines management
          tr
            +ctd ntofdaq-developers
            td
          tr
            +ctd ntof-members
            td
              ul
                li Website "user" role
                li DIMon "user" role
                li Operation-control "user" role
                li Standalone-DAQ "user" role
          tr
            +ctd ntof-experts
            td
              ul
                li Website "admin" role
                li DIMon "expert" role
                li Expert interfaces "user" role
                li Operation-control "user" role
                li Standalone-DAQ "user" role
          tr
            +ctd ntof-shiftleaders
            td
              ul
                li Website "admin" role
                li DIMon "user" role
          tr
            +ctd ntof-admin
            td
              ul
                li Website "admin" role
                li DIMon "expert" role
          tr
            +ctd ntof-daq-newcards-developers
            td
              ul
                li Jira developer access

    section
      name DAQ Machines

      t Here's the list of machines involved in DAQ data-taking:
      table(align='center')
        name DAQ machines
        thead
          tr
            +cth Hostname
            +cth Services
            +cth Managed
            +cth Network Domain
        tbody
          tr #[+ctd ntofapp-1 ntofapp-2]
            +ctd eacs, addh, rawfilemerger, dim-sysmon
            +ctd puppet
            +ctd GPN
          tr #[+ctd ntofproxy-1 ntofproxy-2]
            +ctd
              | alarmserver, bctreader, dim-dns, dim-proxy-*, dim-sysmon,
              | pressurealarm, plcserver
            +ctd puppet
            +ctd TN trusted (GPN with access to TN)
          tr #[+ctd ntofdaq-m{1,2,3,4,6,8}]
            +ctd daqmanager, dim-sysmon
            +ctd puppet
            +ctd GPN
          tr #[+ctd ntofdaq-m12]
            +ctd daqmanager, dim-sysmon, dim-dns, rawfilemerger
            +ctd puppet
            +ctd GPN
          tr #[+ctd ntofanalysis-1 ntofanalysis-2]
            +ctd rawfilemerger
            +ctd puppet
            +ctd GPN
          tr #[+ctd daqmaster]
            +ctd puppetserver
            +ctd
            +ctd GPN
          tr #[+ctd cfp-802-xfil1 (PLC)]
            +ctd FilterStation (EAR1, EAR2)
            +ctd
            +ctd GPN
          tr #[+ctd cfp-802-xear1 (PLC)]
            +ctd Temperatures
            +ctd
            +ctd GPN
          tr #[+ctd VENTnTOFA (PLC)]
            +ctd Vaccum
            +ctd
            +ctd TN
          tr #[+ctd cfp-380-xear2 (PLC)]
            +ctd Temperatures, Water, Vaccum, Doors
            +ctd
            +ctd GPN
          tr #[+ctd cfc-802-xear1 (FEC)]
            +ctd CaenHVBoard_DU, LTIM_DU (timing), PLCGenIO (pressure)
            +ctd CCDE
            +ctd GPN
          tr #[+ctd cfc-380-xear2 (FEC)]
            +ctd CaenHVBoard_DU, LTIM_DU (timing), PLCGenIO (pressure), StepDriver (sample-exchanger)
            +ctd CCDE
            +ctd GPN
          tr #[+ctd cfc-547-xlab (FEC)]
            +ctd LTIM_DU (timing)
            +ctd CCDE
            +ctd GPN

      t
        | #[strong Note:] the #[tt ntofdaq-admins] e-group is used as management
        | account for those devices (unless FEC and PLCs).

      section
        name IPMI monitoring

        t
          | #[tt ntofdaq-*, ntofapp-*, ntofproxy-* and ntofanalysis-*] machines
          | are #[xref(target="IPMI") IPMI] monitored, their interface can be
          | accessed using the following URL scheme: #[tt= 'https://ipmi-<hostname>.cern.ch'].

        t
          | #[strong Note:] IPMI interface is mainly used to access low-level console, and
          | soft/hard reset or power-off a machine, it can be used through DIMon.

      section(anchor='puppet-configuration')
        name Puppet Configuration

        t
          | #[xref(target="PUPPET") Puppet] configuration managed and stored on
          | a dedicated
          | #[eref(target='https://gitlab.cern.ch/ntof/daq/puppet-master-configuration') GitLab repository].

        t
          | #[strong Note:] master branch of this repository is automatically
          | pulled by #[tt daqmaster] (once every quarter) and then applied by
          | managed machines (once every hour whilst idle).

        t
          | #[strong Note:] Modifications on this repository #[bcp14 SHOULD] be made on branches
          | using merge-requests validating modification syntax before being merged
          | on #[tt master] branch.

        t To update #[tt daqmaster] configuration (reducing the quarter delay):
        sourcecode(type="bash")
          | ssh root@daqmaster.cern.ch
          |
          | # Run the update script
          | /etc/puppetlabs/pull.sh

        t To run #[tt puppet-agent] on a managed machine (reducing the hour delay):
        sourcecode(type="bash")
          | ssh ntofdev@ntofapp-1.cern.ch
          |
          | # As used in /etc/cron.hourly/puppetrun
          | sudo flock -xn /run/lock/puppetlock /opt/puppetlabs/bin/puppet agent -t

        t #[strong Note:] puppet may also be ran using #[tt DIMon]

        t
          | #[strong Note:] puppet-agent #[tt 1.10.9] and puppetserver #[tt 2.8.1] are
          | being used when CentOS7-CERN repositories provides a newer version
          | puppet-agent #[tt 6.20.0] (shipping both agent and server), latest
          | LTS opensource version being #[tt 6.26.0].

      section
        name HowTo

        t
          | Here are some commands that may be helpful to inspect DAQ machines
          | (not for PLC or FEC):
        sourcecode(type="bash")
          | # Load ntofdev password in your clipboard
          | pass n_ToF/Users/ntof\ development -c
          |
          | # Accessing a machine (here "ntofapp-1", GPN only)
          | # Note: always use "ntofdev" account
          | ssh ntofdev@ntofapp-1.cern.ch
          |
          | # Inspect a service logs (here "eacs")
          | journalctl -u eacs --since today
          |
          | # Restart a service (here "eacs")
          | sudo systemctl restart eacs

        t
          | #[strong Note:] those machines are managed by puppet, configuration
          | changes are likely to be overriden and users #[bcp14 MUST NOT] achieve
          | any direct system modifications, those #[bcp14 MUST] be done on
          | the #[xref(target='puppet-configuration') puppet-configuration] repository
          | to guarantee that machines can easily be re-deployed.

    section
      name Control Machines

      t Here's the list of configuration and monitoring machines (control-room):
      table(align='center')
        name Control-Room machines
        thead
          tr
            +cth Hostname
            +cth Services
            +cth Applications
            +cth Managed
        tbody
          tr #[+ctd lx-pcntof-gui1 lx-pcntof-gui2]
            +ctd alarmnotifier
            +ctd gecco, operation-control (firefox)
            +ctd puppet
          tr #[+ctd lx-pcntof-flv1 lx-pcntof-flv2]
            +ctd
            +ctd FileDisplay
            +ctd puppet
          tr #[+ctd lx-pcntof-lab547]
            +ctd
            +ctd operation-control (web), FileDisplay
            +ctd puppet
          tr #[+ctd lx-pcntof-monitor]
            +ctd
            +ctd fixed-display (firefox)
            +ctd puppet
          tr #[+ctd lx-pcntof-ear1 lx-pcntof-ear2]
            +ctd
            +ctd website (web)
            +ctd
          tr #[+ctd lx-pcntof-ccmon] #[+ctd] #[+ctd ?] #[+ctd]
          tr #[+ctd lx-pcntof-info] #[+ctd] #[+ctd ?] #[+ctd]
          tr #[+ctd lx-pcntof-op] #[+ctd] #[+ctd ?] #[+ctd]
          tr #[+ctd lx-pcntof-setup] #[+ctd] #[+ctd ?] #[+ctd]
          tr #[+ctd lx-pcntof-vistar] #[+ctd] #[+ctd ?] #[+ctd]

      t
        | #[strong Note:] the #[tt ntofdaq-admins] e-group is used as management
        | account for those devices.

      section
        name HowTo

        t
          | Control Machines can be accessed the same way than DAQ Machines
          | using either #[tt ntofall] or #[tt ntofdev] accounts, only
          | #[tt ntofdev] is allowed to run #[tt sudo] and this is on purpose.

    section
      name Web-Applications

      t
        | #[strong Note:] web-applications are hosted on
        | #[xref(target='CERN_OPENSHIFT') OpenShift] #[strong ntof-daq] project.

      t Here's a list of deployed web-applications:
      table(align='center')
        name Web-Applications
        thead
          tr
            +cth Name
            +cth URL
            +cth Permissions
            +cth Visibility

        tbody
          tr #[+ctd website]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/website')]
                li #[eref(target='https://ntof-daq.web.cern.ch/beta/website')]
                li #[eref(target='https://ntof-daq.web.cern.ch/alpha/website')]
            +ctd Database
            +ctd Internet
          tr #[+ctd standalone-daq]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/standalone')]
                li #[eref(target='https://ntof-daq.web.cern.ch/beta/standalone')]
                li #[eref(target='https://ntof-daq.web.cern.ch/alpha/standalone')]
            +ctd DIM/DAQ Configuration
            +ctd GPN
          tr #[+ctd op-control]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/operation')]
                li #[eref(target='https://ntof-daq.web.cern.ch/beta/operation')]
                li #[eref(target='https://ntof-daq.web.cern.ch/alpha/operation')]
            +ctd DIM/DAQ Configuration, Database
            +ctd GPN
          tr #[+ctd dimon]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/dimon')]
                li #[eref(target='https://ntof-daq.web.cern.ch/beta/dimon')]
                li #[eref(target='https://ntof-daq.web.cern.ch/alpha/dimon')]
            +ctd Machine direct access, IPMI, PLC, OpenShift
            +ctd GPN
          tr #[+ctd fixed-display]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/display')]
                li #[eref(target='https://ntof-daq.web.cern.ch/beta/display')]
            +ctd DIM/DAQ monitoring
            +ctd GPN
          tr #[+ctd cameras]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/cameras')]
            +ctd Cameras access
            +ctd GPN
          tr #[+ctd filter-station]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/expert/filterstation')]
            +ctd DIM/PLC FilterStation access
            +ctd GPN
          tr #[+ctd sample-exchanger]
            td
              ul
                li #[eref(target='https://ntof-daq.web.cern.ch/expert/sample-exchanger')]
            +ctd RDA3/StepDriver access
            +ctd GPN

      section
        name Authentication

        t
          | Web-applications are protected using
          | #[eref(target='https://auth.cern.ch') CERN SSO], all applications are
          | owned by #[tt ntofprod] account and can be administrated by
          | #[tt ntofdaq-admis] e-group members.

      section
        name HowTo

        t
          | Install #[tt oc] command line tool according to
          | #[eref(target='https://paas.cern.ch/command-line-tools')], then
          | do the following:
        sourcecode(type='bash')
          | # Login on openshift server
          | oc login https://api.paas.okd.cern.ch
          |
          | # select the appropriate project
          | oc project ntof-daq

        t
          | #[strong Note:] refer to each application's #[tt README.md] file
          | for instructions on how to deploy its configuration.

        t
          | #[strong Note:] images are automatically pulled every quarter by
          | openshift and deployed when updated on GitLab.

        t Here are some useful commands:
        sourcecode(type='bash')
          | # Force an image check (reduce quarter delay)
          | oc import-image standalone-daq:latest
          |
          | # Restart an application
          | oc rollout restart deployment/dimon-alpha

    section
      name Transfer

      t
        | The n_ToF DAQ project will be transfered to the n_ToF collaboration by
        | doing the following changes (by the end of 01/2022):
      ul
        li Transfer #[tt ntofdev, ntofprod, ntofci] service accounts to collaboration (E. Berthoumieux).
        li Transfer #[tt ntof, ntofdev] database accounts to collaboration (E. Berthoumieux).
        li Removing BE-CEM-MRO members from #[tt ntofdaq-admins] e-group.
        li Adding collaboration members to #[tt ntofdaq-admins] e-group (F. Gunsing, E. Berthoumieux, M. Bacak, G. Vecchio).

      t
        | #[strong Note:] access to the password-store has already been shared
        | with collaboration members.

      t
        | #[strong Note:] BE-CEM-MRO members can be left in #[tt ntof-experts] e-group
        | until 06/2022 to provide second-level support if needed during operation.

  back
    references
      name References
      references
        name Normative References
        | &rfc2119;

      references
        name Informative References

      references
        name URL References
        reference(anchor='nToF_password_store' target='https://gitlab.cern.ch/ntof/daq/password-store')
          front
            title n_ToF password-store
            author
              organization CERN
            date(year='2021')
        reference(anchor='CERN_OPENSHIFT' target='https://paas.cern.ch')
          front
            title CERN Platform As A Service (PAAS)
            author
              organization CERN
            date(year='2021')
        reference(anchor="CERN_ORACLE" target='https://cern.service-now.com/service-portal?id=service_element&name=oracle-database-service')
          front
            title CERN Oracle Database Service
            author
              organization CERN
            date(year='2021')
        reference(anchor="IPMI" target='https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface')
          front
            title Intelligent Platform Management Interface
            author
              organization WikiPedia
        reference(anchor="PUPPET" target='https://puppet.com/docs/puppet')
          front
            title Puppet software configuration management tool 
            author
              organization Puppet, Inc
        reference(anchor='xml2rfc' target='http://xml.resource.org')
          front
            title XML2RFC tools and documentation
            author
              organization RFC Editor
            date(year='2013')

    //- section
    //-   name Contributors
    //-   // Optional section to mention contributors
    //- 
    //- section
    //-   name Change Log
    //-   t A list of changes
    //- 
    //- section
    //-   name Open Issues
    //-   t A list of open issues regarding this document
