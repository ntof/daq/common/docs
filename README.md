# n_ToF DAQ documentation stream

Here are source documents for the n_ToF DAQ documentation stream.

Documents are [pre-published here](http://www.cern.ch/ntofci/docs/).

## Documents generation

To generate documentation it is recommended to use the docker container as described in [xml2rfc README](https://gitlab.cern.ch/mro/common/docs/xml2rfc/blob/master/README.md):
