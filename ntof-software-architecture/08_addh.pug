section(anchor="ADDH")
  name ADDH

  t
    | The #[em ADDH] service is used by the #[xref(target='EACS') EACS] to
    | inject ADDH content in #[tt ".event"] files.

  t
    | This service #[bcp14 MUST] be able to access the #[tt ".event"] files,
    | it #[bcp14 SHOULD] be hosted on the same machine than the
    | #[xref(target='EACS') EACS].

  t
    | The output file #[bcp14 MUST] create the file if it didn't exist,
    | and append data at the end leaving other content untouched.

  section
    name Configuration

    t
      | The #[em ADDH] service #[bcp14 MUST] be configured to subscribe to
      | the various services providing additional header information.
    figure
      name ADDH configuration #[xref(target="RELAX_NG") relax-ng compact schema].
      sourcecode(type='rnc' src='ntof-software-architecture/files/ADDH-Configuration.rnc')

    t
      | The configuration file default location is
      | #[tt "/etc/ntof/nTOF_ADDH.xml"].

    t 
      | #[em ADDH] is exposing a specific command and service (#[xref(target='ADDH_Extra') ADDH/Extra])
      | to dynamically add/remove subscription to various services.
      | The extra configuration file default location is
      | #[tt "/etc/ntof/nTOF_ADDH_extra.xml"]. This configuration is loaded
      | on startup and it's updated every time the extra configuration changes.

  section
    name Supported Types

    t 
      | #[em ADDH] is supporting DataSet and Parameters xml services as described
      | in #[xref(target="dim-xml-extension-01") DIM XML Extension]. It is also
      | supporting basic DIM services. 
    t 
      | Basic DIM services are supported only if they provide a single value or an
      | an array of value of the same type. Structures of mixed types
      | are not supported.

    t DIMData Types (XML DataSet and Parameters) are casted as follows:
    table(align="center")
      name DIMData Types conversion
      thead
        tr #[th DIMData Type]           #[th ADDH Type]
      tbody
        tr #[td DIMData::TypeString]    #[td ADDH::TypeChar]   
        tr #[td DIMData::TypeInt]       #[td ADDH::TypeInt32]   
        tr #[td DIMData::TypeShort]     #[td ADDH::TypeInt32]   
        tr #[td DIMData::TypeLong]      #[td ADDH::TypeInt64]   
        tr #[td DIMData::TypeFloat]     #[td ADDH::TypeFloat]   
        tr #[td DIMData::TypeDouble]    #[td ADDH::TypeDouble]  
        tr #[td DIMData::TypeByte]      #[td Not Supported.]
        tr #[td DIMData::TypeBool]      #[td Not Supported.]
        tr #[td DIMData::TypeUInt]      #[td Not Supported.]
        tr #[td DIMData::TypeULong]     #[td Not Supported.]
        tr #[td DIMData::TypeUByte]     #[td Not Supported.]
        tr #[td DIMData::TypeUShort]    #[td Not Supported.]
        tr #[td DIMData::TypeNested]    #[td Not Supported.]
        tr #[td DIMData::TypeEnum]      #[td Not Supported.]
          
    t DIM Basic values are casted as follows:
    table(align="center")
      name DIM Types conversion
      thead
        tr #[th DIM Service Type]  #[th Description]          #[th ADDH Type]
      tbody
        tr #[td I]    #[td 32 bits signed integer (int32_t)]        #[td ADDH::TypeInt32]
        tr #[td L]    #[td 32 bits signed integer (deprecated)]     #[td ADDH::TypeInt32]
        tr #[td C]    #[td 8 bit character (char)]                  #[td ADDH::TypeChar]
        tr #[td X]    #[td 64 bits signed integer (int64_t)]        #[td ADDH::TypeInt64]
        tr #[td S]    #[td 16 bits signed integer (int16_t)]        #[td ADDH::TypeInt32]
        tr #[td F]    #[td 32 bits floating point number (float)]   #[td ADDH::TypeFloat]
        tr #[td D]    #[td 64 bits floating point number (double)]  #[td ADDH::TypeDouble]

    t
      | DIM Services are described in #[xref(target="dim-protocol-01") DIM Protocol] document.

    t
      | ADDH Types are part of ntoflib library, described by the class #[em AdditionalDataValue]

  section
    name DIM API

    t The ADDH service implements the following DIM services:
    table(align='center')
      name ADDH services
      thead
        tr #[th Service] #[th Description] #[th Clients]
      tbody
        tr
          td ADDH/State
          td #[xref(target="ADDH_State") ADDH service state]
          td EACS
        tr
          td ADDH/Command
          td #[xref(target="ADDH_Command") send a command to the ADDH]
          td EACS, GUIs
        tr
          td ADDH/Event
          td #[xref(target="ADDH_Event") ADDH completion event]
          td EACS
        tr
          td ADDH/Extra
          td #[xref(target="ADDH_Extra") ADDH dynamic configuration]
          td GUIs

    section(anchor="ADDH_State")
      name ADDH/State service

      t
        | This service is a State Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports the ADDH current state.

      t
        | This state machine is really basic and is mostly used to report
        | errors and warnings, it only has two states:
      ul
        li #[tt UNDEFINED(0)] initial state
        li #[tt STARTED(1)] when the ADDH service is running

      t
        | A non exhaustive list of possible errors and warnings:
      table(align="center")
        name ADDH errors
        thead
          tr #[th Error]      #[th Code]  #[th Type]    #[th Description]
        tbody
          tr #[td Ack]        #[td 30]    #[td error]   #[td deprecated will be removed]
          tr #[td CmdUnknown] #[td 32]    #[td error]   #[td command name is not recognized]
          tr #[td CmdError]   #[td 33]    #[td error]   #[td deprecated will be removed]
          tr #[td CmdEmpty]   #[td 34]    #[td error]   #[td command not found in XML document]
          tr #[td WriteErr]   #[td 50]    #[td error]   #[td IO error on file]
          tr #[td NoLink]     #[td 0-x]   #[td warning] #[td service is not reachable ]

    section(anchor="ADDH_Command")
      name ADDH/Command service

      t This service is a Command Service as described in #[xref(target="dim-xml-extension-01") DIM XML Extension].

      figure
        name ADDH/Command Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/ADDH-ADDH_Command_cmd.rnc')

      t
        | The #[tt write] command is asynchronous, it will queue the
        | #[tt ADDH] file writing task and directly return to the caller.

      t
        | An #[xref(target='ADDH_Event') ADDH event] will be sent once the
        | file has been written.

      t
        | The #[tt extra] command will process the new extra configuration content,
        | and it will dynamically add/remove services. The current extra configuration
        | is always published on #[xref(target='ADDH_Extra') ADDH Extra] service.
        | A missing or empty extra node will reset the extra configuration.

    section(anchor="ADDH_Event")
      name ADDH/Event service

      t
        | This service sends an event whenever a #[tt write] command finishes.

      figure
        name ADDH/Event Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/ADDH-ADDH_Event.rnc')

    section(anchor="ADDH_Extra")
      name ADDH/Extra service

      t
        | This service publish the current extra configuration.
        | The content of this service is exactly the extra xml node that is 
        | provided to the extra command.
