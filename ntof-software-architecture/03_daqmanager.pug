section(anchor="DAQMANAGER")
  name DaqManager

  t
    | The DaqManager is the software that controls the high-speed
    | acquisition cards.

  section
    name DIM API

    t The DaqManager implements the following DIM services:
    table(align='center')
      name DaqManager services
      thead
        tr #[th Service] #[th Description]
      tbody
        tr
          td /ListDaqElements
          td #[xref(target="DAQ_ListDaqElements") lists DAQ elements]
        tr
          td /Daq/Command
          td #[xref(target="DAQ_Command") send a command to the DAQ]
        tr
          td /DaqState
          td #[xref(target="DAQ_DaqState") global DAQ state]
        tr
          td /WriterState
          td #[xref(target="DAQ_WriterState") DAQ writer thread state]
        tr
          td /AcquisitionState
          td #[xref(target="DAQ_AcquisitionState") DAQ acquisition thread state]
        tr
          td /WRITER/RunExtensionNumber
          td #[xref(target="DAQ_Writer_RunExtensionNumber") Writer RunExtensionNumber events]
        tr
          td /CARDx/CHANNELx
          td #[xref(target="DAQ_Channel") Channel configuration]
        tr
          td /CARDx/CHANNELx/Calibration
          td #[xref(target="DAQ_Calibration") Channel calibration]
        tr
          td /ZeroSuppression
          td #[xref(target="DAQ_ZeroSuppression") ZeroSuppression configuration]

    section(anchor="DAQ_ListDaqElements")
      name /ListDaqElements Service

      t This service lists available cards and channels.

      figure
        name /ListDaqElements Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/DaqManager-ListDaqElements_service.rnc')

    section(anchor="DAQ_Command")
      name /Daq/Command Service

      t This service is a Command Service as described in #[xref(target="dim-xml-extension-01") DIM XML Extension].

      figure
        name /Daq/Command Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/DaqManager-Daq_Command_cmd.rnc')

    section(anchor="DAQ_DaqState")
      name /DaqState Service

      t
        | This service is a State Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports the current DAQ state.

      figure
        name DaqState state diagram
        artwork(align='center' type='binary-art' src='ntof-software-architecture/files/DaqManager-DaqState.png')

    section(anchor="DAQ_WriterState")
      name /WriterState Service

      t
        | This service is a State Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports the DAQ writer thread state.

      figure
        name WriterState state diagram
        artwork(align='center' type='binary-art' src='ntof-software-architecture/files/DaqManager-WriterState.png')

    section(anchor="DAQ_AcquisitionState")
      name /AcquisitionState Service

      t
        | This service is a State Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports the DAQ acquisition state.

      figure
        name Acquisition state diagram
        artwork(align='center' type='binary-art' src='ntof-software-architecture/files/DaqManager-AcquisitionState.png')

    section(anchor="DAQ_Writer_RunExtensionNumber")
      name /WRITER/RunExtensionNumber Service

      t
        | This service gives some information about the last event that has
        | been written to disk.

      t
        | This service is used by the #[xref(target="RawFileMerger") RawFileMerger]
        | in standalone-mode.

      figure
        name /WRITER/RunExtensionNumber Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/DaqManager-WRITER_RunExtensionNumber.rnc')

    section(anchor="DAQ_Channel")
      name /CARDx/CHANNELx Service

      t
        | This service is a Parameters Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it is used to configure acquisition cards.

      table
        name /CARDx/CHANNELx service parameters
        thead
          tr #[th name] #[th index] #[th description]
        tbody
          tr #[td Is channel enabled?] #[td 1] #[td Used to enable/disable a channel]
          tr #[td Detector type] #[td 2] #[td Detector Name (4 letters)]
          tr #[td Detector id] #[td 3] #[td Detector ID]
          tr #[td Module type] #[td 4] #[td Module/Card type (readonly)]
          tr #[td Channel id] #[td 5] #[td Channel ID]
          tr #[td Module id] #[td 6] #[td Module ID]
          tr #[td Chassis id] #[td 7] #[td Chassis ID]
          tr #[td Stream id] #[td 8] #[td Stream ID (always 1)]
          tr #[td Sample rate] #[td 9] #[td Sample rate in MS/s]
          tr #[td Sample size] #[td 10] #[td Sample size in kS]
          tr #[td Full scale] #[td 11] #[td FullScale in mV]
          tr #[td Delay time] #[td 12] #[td post-trigger delay in samples]
          tr #[td Threshold] #[td 13] #[td ZeroSuppression Threshold in mV]
          tr #[td Threshold sign] #[td 14] #[td ZeroSuppression Threshold sign (negative: 0| positive: 1)]
          tr #[td Zero suppression start] #[td 15] #[td ZeroSuppression start offset]
          tr #[td Offset] #[td 16] #[td ZeroSuppression Threshold offset offset in mV]
          tr #[td Pre samples] #[td 17] #[td ZeroSuppression pre-samples to keep]
          tr #[td Post samples] #[td 18] #[td ZeroSuppression post-samples to keep]
          tr #[td Clock state] #[td 19] #[td Clock state (INTC|EXTC)]
          tr #[td Input impedance] #[td 20] #[td Input impedance]

      figure
        name /CARDx/CHANNELx service parameters example
        sourcecode(type="xml" src="ntof-software-architecture/files/DaqManager-CARDx_CHANNELx_ex.xml")

      t Here is a graphical representation of some of those parameters:
      figure
        name Acquisition Window
        artwork(align='center' type='binary-art' src='ntof-software-architecture/files/DaqManager-AcquisitionWindow.png')

      t
        | Some #[em virtual] parameters are added into graphical user
        | interfaces to simplify the physicists work:
      ul
        li #[strong Lower Limit] the recorded signal acquisition window lower bound, #[tt offset] is computed out of it (Lower Limit = -offset - Full scale / 2).
        li #[strong Time Window] the acquisition time window, #[tt Sample size] is computed out of it (Time Window = Sample size / Sample rate).

      section
        name SPDevices cards

        t
          | SPDevices cards (#[tt Module type] S014 or S412) have some additional
          | constraints:
        ul
          li
            | #[tt Sample size], #[tt Sample rate ] and #[tt Delay time]
            | #[bcp14 MUST] be identical for all channels of a given card.

    section(anchor="DAQ_Calibration")
      name /CARDx/CHANNELx/Calibration Service

      t
        | This service is a DataSet Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it is used to retrieve calibrated values once cards have been
        | calibrated.

      table
        name /CARDx/CHANNELx/Calibration service data
        thead
          tr #[th name] #[th index] #[th type] #[th= "modif. state"] #[th description]
        tbody
          tr #[td Calibrated full scale] #[td 0]  #[td 8 (float)] #[td] #[td calibrated full scale value]
          tr #[td Calibrated offset]     #[td 1]  #[td 8]         #[td] #[td calibrated offset value]
          tr #[td Calibrated threshold]  #[td 2]  #[td 8]         #[td] #[td calibrated zero-suppression threshold]

    section(anchor="DAQ_ZeroSuppression")
      name /ZeroSuppression Service

      t This service is a Parameters Service as described in #[xref(target="dim-xml-extension-01") DIM XML Extension].

      t
        | It is used in addition to Zero Suppression related
        | parameters on each #[xref(target="DAQ_Channel") channel configuration]
        | to define the global ZeroSuppression mode:
      ul
        li #[strong Single Master]: only one channel is used as ZeroSuppression master.
        li #[strong Master]: a set of slave channels is described for each channel, channels not described as slaves are "independent".
        li #[strong Independent]: all channels are independent.

      t
        | #[tt Threshold], #[tt Threshold sign] and
        | #[tt Zero suppression start] parameters are ignored
        | on slave channels, Zero Suppression detection being run on the
        | master channel.

      t
        | #[tt Pre samples] and #[tt Post samples] parameters are the
        | ones set on the slave channel.

      figure
        name /ZeroSuppression Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/DaqManager-ZeroSuppression.rnc')

  section
    name Zero Suppression

    t
      | Zero Suppression is a data filtering algorithm that tries to remove
      | non-significant data (noise) from the records in order to reduce
      | the acquisition files size.

    t It is based on a reference signal level filtering (master channel):
    figure
      name ZeroSuppression introduction
      artwork(align='center' type='binary-art' src='ntof-software-architecture/files/DaqManager-ZeroSuppression.png')

    t
      | Data outside of the #[tt Pre-Samples]/#[tt Post-Samples] and
      | #[tt triggered] range is filtered-out.

  section
    name Acquisition Parameters' Limits

    t The DaqManager introduces certain limitations to the acquisition parameters.
    table(align='center')
      name Impedance limitations
      thead
        tr #[th Card] #[th Limitations]
      tbody
        tr
          td DC240
          td #[tt 50 or 1E6]
        tr
          td DC270
          td #[tt 50 or 1E6]
        tr
          td DC282
          td #[tt 50]
        tr
          td ADQ14
          td #[tt 50]
        tr
          td ADQ412
          td #[tt 50]

    table(align='center')
      name Sample Rate limitations
      thead
        tr #[th Card] #[th Available Values]
      tbody
        tr
          td DC240
          td #[tt 1, 10, 50, 100, 200, 250, 400, 500, 1000, 2000MS/s]
        tr
          td DC270
          td #[tt 10, 50, 100, 200, 250, 400, 500, 1000MS/s]
        tr
          td DC282
          td #[tt 10, 50, 100, 200, 250, 400, 500, 1000, 2000MS/s]
        tr
          td ADQ14
          td #[tt 14.0625, 28.125, 56.25, 112.5, 125, 250, 500, 1000MS/s]
        tr
          td ADQ412
          td #[tt 14.0625, 28.125, 56.25, 112.5, 225, 450, 900, 1800MS/s]

    table(align='center')
      name Full Scale limitations
      thead
        tr #[th Card] #[th Limitations]
      tbody
        tr
          td DC240
          td #[tt &gt; 50mV and &lt; 5000mV]
        tr
          td DC270
          td #[tt &gt; 50mV and &lt; 5000mV]
        tr
          td DC282
          td #[tt &gt; 50mV and &lt; 5000mV]
        tr
          td ADQ14
          td #[tt &gt; 50mV and &lt; 5000mV]
        tr
          td ADQ412
          td #[tt &gt; 100mV and &lt; 5000mV]

    table(align='center')
      name Sample Size limitations
      thead
        tr #[th Card] #[th Limitations]
      tbody
        tr
          td DC240
          td #[tt &lt; 16000kS and not 0]
        tr
          td DC270
          td #[tt &lt; 8000kS and not 0]
        tr
          td DC282
          td #[tt &lt; 32000Ks and not 0]
        tr
          td ADQ14
          td #[tt &lt; 256E6  and not 0]
        tr
          td ADQ412
          td #[tt &lt; 175000kS and not 0]

    table(align='center')
      name Offset limitations
      thead
        tr #[th Card] #[th Limitations]
      tbody
        tr
          td DC240
          td #[tt up to +-20000mV for Full Scale &gt; 500mVpp, else +-2000mV]
        tr
          td DC270
          td #[tt up to +-20000mV for Full Scale &gt; 500mVpp, else +-2000mV]
        tr
          td DC282
          td #[tt up to +-5000mV for Full Scale &gt; 1000mVpp, else +-2000mV]
        tr
          td ADQ14
          td #[tt up to +-2500mV ]
        tr
          td ADQ412
          td #[tt up to +-2500mV ]
