section(anchor="ProxyServer")
  name DIM Proxy Server

  t
    | The DIM Proxy Server is a component that bridges RDA3, DIP and DIM
    | protocols to DIM.

  t
    | This component is split in server/plugin architecture, each plugin
    | being a standalone process comunicating with the server using a
    | #[em boost message_queue].

  section
    name Plugin/Server Communication

    t
      | Plugin to Server communication is achieved using XML encoded messages:
    figure
      name Proxy Server/Plugin messages #[xref(target="RELAX_NG") relax-ng compact schema].
      sourcecode(type='rnc' src='ntof-software-architecture/files/ProxyServer_Communication.rnc')

    t
      | The #[em heartbeat] maximum time is set to 10 seconds,
      | plugins #[bcp14 SHOULD] send an heartbeat message at least once
      | every 5 seconds.

    t
      | An additional message-queue is created by each plugin, where
      | #[em command] messages are sent from the server to the plugin,
      | its name being encoded as: #[em= "queueName + sourceName + 'Cmd'"].

    t
      | Each plugin registers its commands by sending a #[em command]
      | message with no #[em dataset] element on the Server message-queue.

    section
      name RDA Plugin details

      t
        | The acquisition data is always a DataSet as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension].

      t
        | #[em from] field is built accordingly:
        | #[em= "deviceName + '/' + propertyName"].

      t
        | The acquisition DataSet has built-in properties:
      table(align="center")
        name Proxy RDA plugin built-in properties
        thead
          tr #[th Name] #[th DIM Data Type] #[th Index]
        tbody
          tr #[td aqcStamp] #[td 4 (int64)] #[td 0]
          tr #[td cycleStamp] #[td 4 (int64)] #[td 1]
          tr #[td cycleName] #[td 3 (string)] #[td 2]

      t
        | RDA Tags on the property are inserted in the DataSet starting
        | at index 3, any unknown data type is skipped, still incrementing
        | the index.

      t
        | The following types are supported:
      table(align="center")
        name Proxy RDA plugin data types
        thead
          tr #[th RDA Data Type] #[th DIM Data Type]
        tbody
          tr #[td DT_BYTE] #[td 6 (int8)]
          tr #[td DT_SHORT] #[td 7 (int16)]
          tr #[td DT_INT] #[td 1 (int32)]
          tr #[td DT_LONG] #[td 4 (int64)]
          tr #[td DT_FLOAT] #[td 8 (float)]
          tr #[td DT_DOUBLE] #[td 2 (double)]
          tr #[td DT_STRING] #[td 3 (string)]
          tr #[td DT_BOOL] #[td 9 (bool)]

      t
        | Those types #[bcp14 MAY] be used to send commands, in addition
        | to the DIM Enum type that is converted to #[em DT_INT].

    section
      name DIP Plugin Details

      t
        | The acquisition data is always a DataSet as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension].

      t
        | This plugin does not support #[em commands].

      t
        | DipData values are inserted in the DataSet, any unknown data type
        | is skipped, not incrementing the index.

      t
        | The following types are supported:
      table(align="center")
        name Proxy DIP plugin data types
        thead
          tr #[th DIP Data Type] #[th DIM Data Type]
        tbody
          tr #[td TYPE_BYTE] #[td 6 (int8)]
          tr #[td TYPE_SHORT] #[td 7 (int16)]
          tr #[td TYPE_INT] #[td 1 (int32)]
          tr #[td TYPE_LONG] #[td 4 (int64)]
          tr #[td TYPE_FLOAT] #[td 8 (float)]
          tr #[td TYPE_DOUBLE] #[td 2 (double)]
          tr #[td TYPE_STRING] #[td 3 (string)]
          tr #[td TYPE_BOOLEAN] #[td 9 (bool)]


    section
      name DIM Plugin Details

      t
        | The DIM plugin only mirrors XML services as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension].

  section
    name Server Configuration

    t
      | The #[em dim-proxy-server] component #[bcp14 MAY] be configured:
    figure
      name DIM Proxy Server configuration
      sourcecode(type='rnc' src='ntof-software-architecture/files/DIMProxyServer_Configuration.rnc')

  section
    name RDA Plugin Configuration

    t
      | The #[em dim-proxy-rda] component #[bcp14 MAY] be configured
    figure
      name DIM Proxy RDA configuration
      sourcecode(type='rnc' src='ntof-software-architecture/files/DIMProxyRDA_Configuration.rnc')

  section
    name DIP Plugin Configuration

    t
      | The #[em dim-proxy-dip] component #[bcp14 MAY] be configured
    figure
      name DIM Proxy DIP configuration
      sourcecode(type='rnc' src='ntof-software-architecture/files/DIMProxyDIP_Configuration.rnc')

  section
    name DIM Plugin Configuration

    t
      | The #[em dim-proxy-dim] component #[bcp14 MAY] be configured
    figure
      name DIM Proxy DIM configuration
      sourcecode(type='rnc' src='ntof-software-architecture/files/DIMProxyDIM_Configuration.rnc')

  section
    name High Voltage

    t
      | The #[em High Voltage] service is a #[xref(target="ProxyServer") ProxyServer RDA service],
      | that is linked to #[em CaenHVBoard] FESA class.

    t
      | For each board four different services are proxied:
    ul
      li #[strong Acquisition]: A DataSet service publishing latest values for all channels.
      li #[strong BoardInformation]: Detailed information about the HighVoltage board.
      li #[strong OnChannels]: A DataSet publishing vmon/imon values for enabled channels.
      li #[strong Setting]: A Command service to configure channels.

    t
      | The Acquisition service contains information about each channel,
      | each parameter being prefixed with #[em= '"chan" + channelIndex'].

    section
      name Acquisition Details

      t
        | The meaning of each parameter is detailed in
        | #[xref(target="CAENHV_Library") CAEN HV Wrapper] documentation.

      t
        | For each channel a #[em pw] (power) parameter is published
        | as an int32, having the following meaning:
      table(align="center")
        name High Voltage pw
        thead
          tr #[th Value] #[th Meaning]
        tbody
          tr #[td 0] #[td UNKNOWN]
          tr #[td 1] #[td ON]
          tr #[td 2] #[td OFF]

      t
        | For each channel a #[em channelStatusBitfield] is published,
        | this value being a bitfield that can be read as described below:
      table(align="center")
        name High Voltage channelStatusBitField
        thead
          tr #[th Value] #[th Status]
        tbody
          tr #[td 0x01] #[td Channel is on]
          tr #[td 0x02] #[td Channel is ramping up]
          tr #[td 0x04] #[td Channel is ramping down]
          tr #[td 0x08] #[td Channel is in overcurrent]
          tr #[td 0x10] #[td Channel is in overvoltage]
          tr #[td 0x20] #[td Channel is in undervoltage]
          tr #[td 0x40] #[td Channel is in external trip]
          tr #[td 0x80] #[td Channel is in max V]
          tr #[td 0x100] #[td Channel is in external disable]
          tr #[td 0x200] #[td Channel is in internal trip]
          tr #[td 0x400] #[td Channel is in calibration error]
          tr #[td 0x800] #[td Channel is unplugged]
          tr #[td 0x2000] #[td Channel is in OverVoltage Protection]
          tr #[td 0x4000] #[td Channel is in Power Fail]
          tr #[td 0x8000] #[td Channel is in Temperature Error]

      t
        | For each channel a #[em caenErrorCode] and a #[em caenErrorCodeMessage]
        | are published:
      table(align="center")
        name High Voltage caenErrorCode
        thead
          tr #[th caenErrorCode] #[th caenErrorCodeMessage]
        tbody
          tr #[td -1] #[td CAENHVCRATE_NOT_IMPLEMENTED]
          tr #[td -2] #[td CAENHVCRATE_COMMAND_NOT_PERMITTED]
          tr #[td -3] #[td CAENHVCRATE_BOARD_NOT_PRESENT]
          tr #[td -4] #[td CAENHVCRATE_CHANNEL_NOT_PRESENT]
          tr #[td -5] #[td CAENHVCRATE_PARAMETER_NOT_PRESENT]
          tr #[td -6] #[td CAENHVCRATE_GROUP_NOT_PRESENT]
          tr #[td -7] #[td CAENHVCRATE_OUT_OF_RANGE]
          tr #[td -8] #[td CAENHVCRATE_READ_ONLY_PARAMETER]
          tr #[td -9] #[td CAENHVCRATE_WRONG_FORMAT]
          tr #[td -10] #[td CAENHVCRATE_SYSTEM_BUSY]
          tr #[td -11] #[td CAENHVCRATE_SYSTEM_ERROR]
          tr #[td -12] #[td CAENHVCRATE_BAD_LOGIN]
          tr #[td -13] #[td CAENHVCRATE_CONNECTION_REFUSED]
          tr #[td -15] #[td CAENHVCRATE_ACCESS_DENIED]
          tr #[td 1] #[td CAENHV_SYSERR]
          tr #[td 2] #[td CAENHV_WRITEERR]
          tr #[td 3] #[td CAENHV_READERR]
          tr #[td 4] #[td CAENHV_TIMEERR]
          tr #[td 5] #[td CAENHV_DOWN]
          tr #[td 6] #[td CAENHV_NOTPRES]
          tr #[td 7] #[td CAENHV_SLOTNOTPRES]
          tr #[td 8] #[td CAENHV_NOSERIAL]
          tr #[td 9] #[td CAENHV_MEMORYFAULT]
          tr #[td 10] #[td CAENHV_OUTOFRANGE]
          tr #[td 11] #[td CAENHV_EXECCOMNOTIMPL]
          tr #[td 12] #[td CAENHV_GETPROPNOTIMPL]
          tr #[td 13] #[td CAENHV_SETPROPNOTIMPL]
          tr #[td 14] #[td CAENHV_PROPNOTFOUND]
          tr #[td 15] #[td CAENHV_EXECNOTFOUND]
          tr #[td 16] #[td CAENHV_NOTSYSPROP]
          tr #[td 17] #[td CAENHV_NOTGETPROP]
          tr #[td 18] #[td CAENHV_NOTSETPROP]
          tr #[td 19] #[td CAENHV_NOTEXECOMM]
          tr #[td 20] #[td CAENHV_SYSCONFCHANGE]
          tr #[td 21] #[td CAENHV_PARAMPROPNOTFOUND]
          tr #[td 22] #[td CAENHV_PARAMNOTFOUND]
          tr #[td 23] #[td CAENHV_NODATA]
          tr #[td 24] #[td CAENHV_DEVALREADYOPEN]
          tr #[td 25] #[td CAENHV_TOOMANYDEVICEOPEN]
          tr #[td 26] #[td CAENHV_INVALIDPARAMETER]
          tr #[td 27] #[td CAENHV_FUNCTIONNOTAVAILABLE]
          tr #[td 28] #[td CAENHV_SOCKETERROR]
          tr #[td 29] #[td CAENHV_COMMUNICATIONERROR]
          tr #[td 30] #[td CAENHV_NOTYETIMPLEMENTED]
          tr #[td 0x1001] #[td CAENHV_CONNECTED]
          tr #[td 0x1002] #[td CAENHV_NOTCONNECTED]
          tr #[td 0x1003] #[td CAENHV_OS]
          tr #[td 0x1004] #[td CAENHV_LOGINFAILED]
          tr #[td 0x1005] #[td CAENHV_LOGOUTFAILED]
          tr #[td 0x1006] #[td CAENHV_LINKNOTSUPPORTED]
          tr #[td 0x1007] #[td CAENHV_USERPASSFAILED]

  section(anchor='SampleExchanger')
    name Sample Exchanger

    t
      | The #[em Sample Exchanger] service is a #[xref(target="ProxyServer") ProxyServer RDA service],
      | that is linked to #[em StepDriver] FESA class.

    t
      | Two properties are proxied:
    ul
      li #[strong Acquisition]: A DataSet service publishing device state (use by EACS to monitor positionNumber).
      li #[strong PredefinedPosition]: A Command service to send movement orders.

    t
      | Whenever #[xref(target='SampleExchanger') sample-exchanger] is configured
      | on #[xref(target='EACS') EACS]
      | and no sample is configured in database, the device #[bcp14 MUST] be
      | moved to the first (positionNumber=0, databaseIndex=0) preferred position.

    t
      | Indexes from database #[bcp14 MUST NOT] be shifted (databaseIndex 1 -&gt; preferredNumber 1),
      | this is to reserve positionNumber=0 to a beam-safe position.