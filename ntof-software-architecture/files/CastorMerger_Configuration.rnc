element castor {
  element oracle {
    # Global config override, since castor merger requires some special permissions
    attribute user { xsd:string },
    attribute password { xsd:string },
    attribute server { xsd:string }
  } ?,
  element paths {
    # Castor file path
    element remote { attribute path { xsd:string } },
    #  Local path to find .run and .event file created by EACS
    element local { attribute path { xsd:string } },
    # Where to look for DAQ data
    # (defaults to /mnt, leading to /mnt/ntofdaq-m1...)
    element mount { attribute path { xsd:string } }
  } ?,
  element castor {
    attribute node { xsd:string },      # required, ex: "castorpublic"
    attribute svcclass { xsd:string } ? # defaults to "default"
  },
  element limits {
    # Castor file size split trigger
    attribute size { xsd:string } ?,        # defaults 2G
    # Castor file event count split trigger
    attribute count { xsd:unsignedInt } ?,  # defaults 50
    # Per file retries count
    attribute retries { xsd:unsignedInt } ? # defaults 1000
  } ?,
  element replace {
    # File conflict merge strategy
    attribute strategy { "RENAME"|"DELETE" }
  } ?,
  element DIM {
    # DIM service prefix
    attribute PREFIX { xsd:string },       # defaults to "CASTOR"
    # Max number of "copied" files to keep (+ merged and ignored)
    attribute history { xsd:unsignedInt }, # defaults to 10
    # Max number of errors active in CastorMerger
    attribute errors { xsd:unsignedInt }   # defaults to 20
  } ?,
  element buffer {
    # Merger task buffer size in MB
    # Internally set to 1MB if not set or set to 0
    attribute size { xsd:unsignedInt } # defaults to 0
  } ?,
  element pool {
    # Number of parallel transfers
    attribute size { xsd:unsignedInt } # defaults to 20
  } ?
}
