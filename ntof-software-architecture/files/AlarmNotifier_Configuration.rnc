start = alarms

actionsDef = (
  element actions {
    element action {
      (
        attribute name { "command" },
        # it spawns a command in a shell (see system(3) for details)
        attribute command { xsd:string }
      ) | (
        attribute name { "speech" },

        # speech mode is a mix of text and variables.
        # The alarm notifier will read the text
        # and the variables value in order to compose a sentence.
        # variable will try to extract information from, for example, a dim xml state service

        # Example for service EACS/State that is in OK State.
        # <text>"The EACS State is"</text>
        # <variable from="attribute" name="strValue" xpath=".")
        # <text>". End of speech."</text>

        # This will result in synthesizing "The EACS State is OK. End of speech." on the main audio output

        element text { xsd:string } *,
        element variable {
          attribute from { "attribute" | "node" },
          attribute name { xsd:string } ?,
          attribute xpath { xsd:string } ?
        } *
      ) | (
        attribute name { "sound" },

        attribute file { xsd:string },
        # if enqueue is 0 it will not be played if the player is busy.
        attribute enqueue { xsd:int } ? # 1 by default
      )
    } *
  }
)

alarmDef = (
  element alarm {
    # alarm attributes filters
    attribute ident { xsd:string } ?,
    attribute system { xsd:string } ?,
    attribute subsystem { xsd:string } ?,
    attribute severity { xsd:string } ?,

    # throttling attribute name (default: ident)
    attribute throttle { xsd:string } ?,
    actionsDef
  }
)


alarms = element alarms {

  element configuration {
    element global {

      element dimDns { attribute value { xsd:string } },

      # actionsCfg is implemented only for speech actions to
      # initialize festival library by executing specific commands.
      element actionsCfg {
        element action {
          attribute name { xsd:string },
          element command { attribute value { xsd:string } } *
        } *
      } ?
    },
    element timing {
      attribute service { xsd:string },

      element values {
        element value {
          attribute name { xsd:string },
          actionsDef
        }
      } *

    } ?,

    element states {
      element state {
        attribute service { xsd:string },

        element all {
          actionsDef *
        } ?,
        element default {
          actionsDef *
        } ?,
        element values {
          element value {
            attribute enum { xsd:int },
            actionsDef *
          } *
        }
      }

    } ?,

    element alarms {
      attribute service { xsd:string } ?,

      # Alarm throttling value in milliseconds
      attribute lastActive { xsd:long } ?,

      element ignore {
        alarmDef *
      } ?,
      element override {
        alarmDef *
      } ?,
      element default {
        alarmDef *
      } ?

    }
  }
}
