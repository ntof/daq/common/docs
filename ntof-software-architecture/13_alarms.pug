section
  name Alarms
  t
    | This section explains how the alarm system works and all the component that are involved.

  section(anchor='AlarmServer')
    name AlarmServer
    t
      | This component connects on different kind of DIM Services and can monitor their status and values.
      | In particular, it can be configured, as explained in the #[xref(target="AlarmServerConfig")]
      | to use different DIM providers:

    ul
      li #[tt DIMon]: It monitors DIM services (No Link).
      li #[tt DIMState]: It monitors DIM States services (No Link) and errors/warnings.
      li
        | #[tt DIMDataSet]: It monitors DIM DataSet services (No Link) and specific DIMData value that
        | can be configured with min/max thresholds.

    t
      | This component allows to connect and register external alarms using interprocess
      | communication (by using #[tt boost::interprocess::message_queue])
      | by sending a correct serialized form of the alarm configuration. Serialized form is:
      | #[em "identifier active lastActive masked disabled"]. Where:

    ul
      li #[tt identifier]: (string) an unique identifier of the alarm
      li #[tt active]: (boolean) is alarm currently active?
      li #[tt lastActive]: (int64) timestamp when the alarm was active last (in milliseconds since epoch)
      li #[tt masked]: (boolean) is alarm masked?
      li #[tt disabled]: (boolean) is alarm disabled?

    t
      | Examples: "customAlarm 0 1603876687 0 1" for a not active and disabled alarm,
      | "customAlarm 1 1603876687 0 0" for a currently active alarm.

    t
      | This component publishes alarms information in different ways:

    ul
      li on a #[strong DIM XML Service] (by default under #[tt /ntof-alarms/Alarms])
      li on a #[strong WebSocket server] (by default listening on port #[tt 8888])

    t
      | All these 2 services are able to provide information about alarms in the following xml format:

    figure
      name /ntof-alarms/Alarms Service #[xref(target="RELAX_NG") relax-ng compact schema].
      sourcecode(type='rnc' src='ntof-software-architecture/files/AlarmServer_alarms_schema.rnc')

    t
      | The #[strong WebSocket] is reachable by default on port 8888 (ws://hostname:8888). On connect, the client receive a message with
      | the current information about the alarms as shown in the previous schema.
      | The WebSocket connection remains active and the server will send update every time there is a change in the
      | states of alarms.

    t
      | This component offer 2 ways in order to interact with the AlarmServer and mask/disable alarms:

    ul
      li on a #[strong DIM XML Command] (by default under #[tt /ntof-alarms/Alarms/Cmd])
      li on a #[strong HTTP server] (by default listening on port #[tt http://hostname:8888/command?parameters])

    t
      | The DIM Command accept the following commands:

    figure
      name Alarms/Cmd service #[xref(target="RELAX_NG") relax-ng compact schema].
      sourcecode(type='rnc' src='ntof-software-architecture/files/AlarmServer_Cmd_cmd.rnc')

    t
      | The Web Server is listening also on HTTP protocol (http://hostname:8888). You can send query parameters in
      | order to interact with the alarm server:

    ul
      li
        | #[tt /mask?ident]: toggle masking attribute of the identified alarm. A message by the server is sent
        | with the result of this action (ok or error).
      li
        | #[tt /disable?ident]: toggle disable attribute of the identified alarm. A message by the server is sent
        | with the result of this action (ok or error).
      li #[tt /reload]: clears everything and restart alarm server by reloading the configuration
      li #[tt /get?all|masked|disabled]: retrieve the alarms information (all by default if no parameters are provided)

    section(anchor='AlarmServerConfig')
      name Configuration

      t
        | The #[em Alarm Server] application #[bcp14 SHOULD] be configured accordingly:
      figure
        name AlarmServer Configuration #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/AlarmServer_Configuration.rnc')

      t The configuration file default location is
        | #[tt "/etc/ntof/alarmServer.xml"].

      t
        | The dim dns information is retrieved from the #[xref(target='GLOBAL_CONFIG') global configuration] file.

  section(anchor='PressureAlarm')
    name PressureAlarm
    t
      | The pressure alarm component monitors Pressure Sensors, implemented by FESA class,
      | by using #[xref(target="ProxyServer") DIM Proxy Plugin]
      | and then publishes a DIM State Service as described in #[xref(target="dim-xml-extension-01") DIM XML Extension]
      | using errors/warnings to reflect the current alarm level based on min and max thresholds.

    t
      | If thresholds are not provided by the configuration, they are automatically retrieved from the proxy service.
      | Moreover, It ensures that a given pressure value is within a configure threshold range,
      | moving the DIM State service in ERROR state when out of range for a certain delay.

    t
      | This could have been implemented by using directly the DIMDataSet monitoring, but this component allows
      | to easily configure a list of pressure alarms and brings a certain level of automation.

    section(anchor='PressureAlarmConfig')
      name Configuration

      t
        | The #[em Pressure Alarm] application #[bcp14 SHOULD] be configured accordingly.

      figure
        name PressureAlarm Configuration #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/PressureAlarm_Configuration.rnc')

      t The configuration file default location is
        | #[tt "/etc/ntof/pressureAlarm.xml"].

      t
        | The dim dns information is retrieved from the #[xref(target='GLOBAL_CONFIG') global configuration] file.


  section(anchor='AlarmNotifier')
    name AlarmNotifier
    t
      | This application is an utility that monitors:

    ul
      li #[tt DIM State services]: as described in #[xref(target="dim-xml-extension-01") DIM XML Extension]
      li #[tt Timing service]: as described in #[xref(target="TIMING") the Timing chapter]
      li #[tt Alarms by AlarmServer]: as described in #[xref(target="AlarmServer") the AlarmServer chapter]

    t
      | AlarmNotifier can be configured to react
      | by executing a shell commands, reading a text and playing a sound.
      | In particular the possibilities are:

    ul
      li #[tt command]: execute a shell command that can be configured
      li #[tt speech]: read a configured text with variables
      li #[tt sound]: emit a sound by reading a file specified in the configuration

    t
      | It's mainly used in the Control Room to inform shifters that an event occurred.

    section
      name Configuration

      t
        | The #[em Alarm Notifier] application #[bcp14 SHOULD] be configured in order to listen
        | on alarms and emit sounds.

      figure
        name AlarmNotifier Configuration #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/AlarmNotifier_Configuration.rnc')

      t The configuration file default location is
        | #[tt "/etc/ntof/nTOF_AlarmNotifier.xml"].

      t Here an example of AlarmNotifier configuration:

      figure
        name AlarmNotifier Configuration XML example.
        sourcecode(type='xml' src='ntof-software-architecture/files/AlarmNotifier_Configuration_Example.xml')
