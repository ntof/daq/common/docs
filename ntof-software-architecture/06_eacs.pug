section(anchor="EACS")
  name EACS

  t
    | The #[em EACS] service is used as a scheduler to synchronize all
    | components together.

  t
    | It #[bcp14 MUST] be the main entry point for the #[xref(target='GUI') GUI].

  section
    name Configuration

    t
      | The #[em EACS] service #[bcp14 SHOULD] be configured to define
      | thresholds and timeout values.
    figure
      name EACS Configuration #[xref(target="RELAX_NG") relax-ng compact schema].
      sourcecode(type='rnc' src='ntof-software-architecture/files/EACS_Configuration.rnc')

    t The configuration file default location is
      | #[tt "/etc/ntof/eacs.xml"].

    t
      | The list of available hardware and services is retrieved from the
      | #[xref(target='GLOBAL_CONFIG') global configuration] file.

  section
    name DIM API

    t The EACS service implements the following DIM services
    table(align='center')
      name DIM services
      thead
        tr #[th Service] #[th Description]
      tbody
        tr
          td EACS/Cmd
          td #[xref(target="EACS_Command") EACS Control]
        tr
          td EACS/Config
          td #[xref(target="EACS_Config") EACS configuration service]
        tr
          td EACS/Info
          td #[xref(target="EACS_Info") EACS general information]
        tr
          td EACS/State
          td #[xref(target="EACS_State") EACS service state]
        tr
          td EACS/RunInfo
          td #[xref(target="EACS_RunInfo") EACS Current Run Information]
        tr
          td EACS/RunConfig
          td #[xref(target="EACS_RunConfig") EACS next run configuration]
        tr
          td EACS/Daq/List
          td #[xref(target="EACS_Daq_List") EACS Daqs introspection service]
        tr
          td EACS/Daq/&lt;name&gt;/CARDx/CHANNELx
          td #[xref(target="EACS_Daq_Chan") EACS Daq channel configuration]
        tr
          td EACS/Daq/&lt;name&gt;/ZeroSuppression
          td #[xref(target="EACS_Daq_ZSP") EACS Daq zero-suppression configuration]
        tr
          td EACS/Events
          td #[xref(target="EACS_Events") EACS last received events information]
        tr
          td EACS/Validation
          td #[xref(target="EACS_Validation") EACS validation notifications service]
        tr
          td EACS/Timing
          td #[xref(target="EACS_Timing") Last received Event information]
        // tr
          td EACS/HighVoltage
          td #[xref(target="EACS_HV") High Voltage]
        // tr
          td EACS/Alarms
          td #[xref(target="EACS_Alarms") Alarms threshold configuration]


    section(anchor="EACS_Command")
      name EACS/Cmd control service

      t This service is a Command Service as described in #[xref(target="dim-xml-extension-01") DIM XML Extension].

      t It is used to control the EACS and supports the following commands:
      table
        thead
          tr #[th Command] #[th Valid States] #[th Description]
        tbody
          tr #[td start] #[td IDLE] #[td start a run]
          tr #[td stop] #[td #[strong not] IDLE, LOADING or ERROR] #[td stop the run. By default dataStatus is #[tt unknown]]
          tr #[td reset] #[td IDLE, ERROR] #[td reset the EACS state and configuration]
          tr #[td config] #[td IDLE] #[td load a configuration]

      t
        | The #[em config] command is equivalent to #[em setConfig] call
        | on #[xref(target="EACS_Config") EACS/Config] service.

      figure
        name EACS/Cmd service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/EACS_Cmd_cmd.rnc')

    section(anchor="EACS_Config")
      name EACS/Config service

      t
        | This service is an XML/RPC service as described in
        | #[xref(target='dim-xml-extension-01') DIM XML Extension], it
        | is used to set or get the complete EACS configuration.

      figure
        name EACS/RpcIn
        sourcecode(type='rnc' src='ntof-software-architecture/files/EACS_Config_RpcIn.rnc')

      t
        | The #[em getConfig] returns the complete EACS configuration,
        | as a list of #[em service] elements that contains the parameters
        | of each EACS service.

      t
        | The #[em setConfig] can set the complete EACS configuration or
        | only parts, it is up to the caller to specify only part or
        | the all parameters at once.

      t
        | Configuring a non-existent service #[bcp14 SHOULD NOT] fail,
        | it #[bcp14 MAY] report a warning (ex: reloading a configuration,
        | but some daqs have been removed).

      t
        | Calling #[em setConfig] in any other state than #[em IDLE],
        | #[bcp14 MUST] be rejected.

    section(anchor="EACS_Info")
      name EACS/Info service

      t
        | This service is a DataSet service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports the EACS general configuration information.

      figure
        name EACS general information
        sourcecode(type='rnc' src='ntof-software-architecture/files/EACS_Info.rnc')

    section(anchor="EACS_State")
      name EACS/State service

      t
        | This service is a State Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports the current EACS state.

      figure
        name EACS state diagram
        artwork(align='center' type='binary-art' src='ntof-software-architecture/files/EACS-ServiceStateMachine.png')

    section(anchor="EACS_RunInfo")
      name EACS/RunInfo service

      t
        | This service is a Parameters Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports information about the ongoing acquisition.

      table
        name EACS/RunInfo service data
        thead
          tr #[th name] #[th index] #[th type] #[th= "modif. state"] #[th description]
        tbody
          tr #[td runNumber]        #[td 1]  #[td 11 (uint32)] #[td] #[td run number]
          tr #[td eventNumber]      #[td 2]  #[td 11]          #[td] #[td event number]
          tr #[td validatedNumber]  #[td 3]  #[td 11]          #[td] #[td Number of events validated]
          tr #[td validatedProtons] #[td 4]  #[td 8 (float)]   #[td]      #[td Total number of validated protons]
          tr #[td title]            #[td 5]  #[td 3 (string)]  #[td *]    #[td Run title]
          tr #[td description]      #[td 6]  #[td 3]           #[td *]    #[td Run description]
          tr #[td area]             #[td 7]  #[td 3]           #[td]      #[td= "Area identification (ex: EAR1, LAB ...)"]
          tr #[td experiment]       #[td 8]  #[td 3]           #[td] #[td Experiment name, will be used to archive files]
          tr #[td detectorsSetupId] #[td 9]  #[td 4 (int64)]   #[td] #[td Detectors setup id]
          tr #[td materialsSetupId] #[td 10] #[td 4]           #[td] #[td Materials setup id]

      t
        | Parameters modifications #[bcp14 SHOULD] be allowed only on the
        | above mentioned states, parameters where the #[em modifiable state]
        | column is left blank are read-only.

      t
        | Starting a run with no #[em materialsSetupId] or no #[em detectorSetupId]
        | #[bcp14 MUST] result in an error.

    section(anchor="EACS_RunConfig")
      name EACS/RunConfig service

      t
        | This service is a Parameters Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports and allow configuration of next acquisition.

      table
        name EACS/NextRunConfig service data
        thead
          tr #[th name] #[th index] #[th type] #[th= "modif. state"] #[th description]
        tbody
          tr #[td runNumber]        #[td 1]  #[td 11 (uint32)] #[td] #[td run number]
          tr #[td title]            #[td 5]  #[td 3 (string)]  #[td *]    #[td Run title]
          tr #[td description]      #[td 6]  #[td 3]           #[td *]    #[td Run description]
          tr #[td experiment]       #[td 8]  #[td 3]           #[td *] #[td Experiment name, will be used to archive files]
          tr #[td detectorsSetupId] #[td 9]  #[td 4 (int64)]   #[td *] #[td Detectors setup id]
          tr #[td materialsSetupId] #[td 10] #[td 4]           #[td *] #[td Materials setup id]

      t
        | Parameters modifications #[bcp14 SHOULD] be allowed only on the
        | above mentioned states, parameters where the #[em modifiable state]
        | column is left blank are read-only.

      t
        | #[em detectorsSetupId] and #[em materialsSetupId] parameters
        | #[bcp14 SHOULD] default to #[em -1],
        | indicating that the value is not yet configured.

      t
        | Starting a run with no #[em materialsSetupId] or no #[em detectorSetupId]
        | #[bcp14 MUST] result in an error.

    section(anchor="EACS_Daq_List")
      name EACS/Daq/List service

      t
        | This service is a DataSet Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it lists available Daq services.

      t
        | #[xref(target="EACS_Daq_Chan")= "EACS/Daq/<name>/CARDx/CHANNELx"] and
        | #[xref(target="EACS_Daq_ZSP")= "EACS/Daq/<name>/ZeroSuppression"]
        | services #[bcp14 MUST] match the ones described in this service.

      figure
        name EACS Daq List service
        sourcecode(type='rnc' src='ntof-software-architecture/files/EACS_Daq_List.rnc')

      figure
        name EACS Daq List service example
        sourcecode(type='XML' src='ntof-software-architecture/files/EACS_Daq_List.xml')

      t
        | The #[em used] data element is optional and #[bcp14 MAY] be
        | omitted when false, it #[bcp14 MUST] be set on cards and
        | channels that are actually used.

      t
        | The #[tt channelsCount], #[tt type] and #[tt serialNumber] elements
        | references elements in
        | #[xref(target="DAQ_ListDaqElements") lists DAQ elements] service,
        | respectively #[tt nbChannel], #[tt type] and #[tt serialNumber]

    section(anchor="EACS_Daq_Chan")
      name= "EACS/Daq/<name>/CARDx/CHANNELx service"

      t
        | This service has the same content than the
        | #[xref(target="DAQ_Channel") Daq Channel] service,
        | it is used to prepare each channel configuration.

      t
        | Modifications on this service in any other state than #[em IDLE]
        | #[bcp14 MUST] be rejected.

    section(anchor="EACS_Daq_ZSP")
      name EACS/Daq/&lt;name&gt;/ZeroSuppression

      t
        | This service has the same content than the
        | #[xref(target="DAQ_ZeroSuppression") Daq ZeroSuppression] service,
        | it is used to prepare each card's zeroSuppression configuration.

      t
        | Modifications on this service in any other state than #[em IDLE]
        | #[bcp14 MUST] be rejected.

    section(anchor="EACS_Events")
      name EACS/Events service

      t
        | This service is a DataSet Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it displays the last events managed by the EACS.

      figure
        name EACS Events service
        sourcecode(type='rnc' src='ntof-software-architecture/files/EACS_Events.rnc')

      t
        | This service #[bcp14 MUST] display up to #[em eventsHistorySize] as
        | set in #[xref(target="EACS_Config") Configuration file].

    section(anchor="EACS_Validation")
      name EACS/Validation service

      t
        | This service is a DataSet Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it sends event validation notifications.

      figure
        name EACS Validation service
        sourcecode(type='rnc' src='ntof-software-architecture/files/EACS_Validation.rnc')

    section(anchor="EACS_Timing")
      name EACS/Timing service

      t
        | This service has the same content than the
        | #[xref(target="TIMING_Params") Timing] service,
        | it is used to prepare its configuration.

      t
        | Modifications on this service in any other state than #[em IDLE]
        | #[bcp14 MUST] be rejected.
