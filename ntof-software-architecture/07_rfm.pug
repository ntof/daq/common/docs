section(anchor="RawFileMerger")
  name RawFile Merger

  t
    | The #[em RawFile Merger] service is used to merge acquisition files in
    | a single stream of files, that will be split according to the number
    | of events or size they reach.

  t
    | Once complete files are then copied and archived using #[xref(target='CTA') CERN Tape Archive]
    | tape storage.

  section
    name File state

    t
      | A file can be in the following states:
    ul
      li #[strong INCOMPLETE] file is not yet finished, some data can be appended.
      li #[strong WAITING_FOR_APPROVAL] file is pending for approval.
      li #[strong WAITING] file is waiting to be transferred on CTA.
      li #[strong TRANSFERRING] the file is being transferred to CTA.
      li #[strong COPIED] the file has been copied to CTA, but not yet migrated on tape.
      li #[strong MIGRATED] the file has been migrated on tape.
      li #[strong IGNORED] the file should not be copied to CTA.
      li #[strong FAILED] the file transfer failed somehow, further attempts may be achieved.

  section
    name File conflicts handling

    t
      | The RawFileMerger supports two different file conflict handling
      | strategies:
    ul
      li
        | #[strong RENAME]: old file is renamed with #[tt ".old"] suffix,
        | or #[tt= '".old[0-9]+"'] incrementing a counter until it finds a
        | free slot.
      li #[strong DELETE]: old file is simply deleted.

    t #[strong Note:] empty files are always deleted.

  section
    name Configuration

    t
      | The #[em RawFileMerger] service #[bcp14 MAY] be configured:
    figure
      name RawFileMerger configuration
      sourcecode(type='rnc' src='ntof-software-architecture/files/RawFileMerger_Configuration.rnc')

    t
      | The configuration file default location is
      | #[tt "/etc/ntof/merger.xml"].

  section
    name DIM API

    t The RawFileMerger service implements the following DIM services
    table(align='center')
      name DIM services
      thead
        tr #[th Service] #[th Description]
      tbody
        tr
          td MERGER/State
          td #[xref(target='MERGER_State') RawFile Merger service state]
        tr
          td MERGER/Cmd
          td #[xref(target='MERGER_Cmd') RawFile Merger command service]
        tr
          td MERGER/Info
          td #[xref(target='MERGER_Info') RawFile Merger stats and info service]
        tr
          td MERGER/Runs
          td #[xref(target='MERGER_Runs') RawFile Merger runs info service]
        tr
          td MERGER/Current
          td #[xref(target='MERGER_Current') RawFile Merger current run info service]
        tr
          td MERGER/Current/Files
          td #[xref(target='MERGER_Current_Files') RawFile Merger current run files info service]
        tr
          td MERGER/Files RPC
          td #[xref(target='MERGER_Files_RPC') RawFile Merger files info RPC]

    section(anchor="MERGER_State")
      name MERGER/State service

      t
        | This service is a State Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it reports the current RawFile Merger state.

      t
        | This state machine is really basic and is mostly used to report
        | errors and warnings, it has only one state (in addition to the
        | builtin states): #[tt OK(0)]
      t
        | A non exhaustive list of possible errors and warnings:
      table(align="center")
        name RawFile Merger errors
        thead
          tr #[th Error] #[th Code] #[th Type] #[th Description]
        tbody
          tr #[td= "<filename>"] #[td incremental] #[td warning] #[td some warning about a specific file]
          tr #[td= "<filename>"] #[td incremental] #[td error] #[td some error about a specific file]
          tr #[td CmdError] #[td 1] #[td error] #[td received command error]
          tr #[td runCompletionChecker] #[td incremental] #[td error] #[td something went wrong in the runCompletionChecker thread]
          tr #[td Deleter] #[td incremental] #[td error] #[td something went wrong in the FileDeleter thread]

      t
        | The maximum number of active warnings and errors is limited
        | dropping incoming errors and warnings when there's already too
        | much (default limit: #[tt 20]).

    section(anchor="MERGER_Cmd")
      name MERGER/Cmd service

      t This service is a Command Service as described in #[xref(target="dim-xml-extension-01") DIM XML Extension].

      figure
        name MERGER/Cmd Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/RawFileMerger_Cmd_cmd.rnc')

      t
        | #[strong Note:] A run can be pre-#[tt approved] upon creation, but
        | it then also #[bcp14 MUST] have its #[tt experiment] set.

      t
        | #[strong Note:] When an #[tt event] is received on an unknown or
        | stopped run it #[bcp14 MUST] be ignored.

      t
        | #[strong Note:] Calling #[tt runDelete] entirely wipes existing
        | files and information relative to a specific run.

    section(anchor="MERGER_Info")
      name MERGER/Info service

      t
        | This service is a DataSet Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it provides global information about the RawFileMerger state.

      figure
        name MERGER/Info Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/RawFileMerger_Info.rnc')

    section(anchor="MERGER_Runs")
      name MERGER/Runs service

      t
        | This service is a Parameters Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it provides overall information about runs.

      figure
        name MERGER/Runs Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/RawFileMerger_Runs.rnc')

      t
        | The following parameters #[bcp14 MAY] be modified:
      table(align="center")
        name Files modifiable parameters
        thead
          tr #[th Parameter] #[th Constraints]
        tbody
          tr
            td experiment
            td As long as the run is not yet #[em approved].
          tr
            td approved
            td
              | Can be set to #[tt "1"], can not be set back to #[tt "0"],
              | #[em experiment] #[bcp14 MUST] be set (may be set in the same command).

    section(anchor="MERGER_Current")
      name MERGER/Current service

      t
        | This service is a Parameters Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it is the same as #[em MERGER/Runs] but provides information about current run only.

      figure
        name MERGER/Current Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/RawFileMerger_Current.rnc')

      t
        | The following parameters #[bcp14 MAY] be modified:
      table(align="center")
        name Files modifiable parameters
        thead
          tr #[th Parameter] #[th Constraints]
        tbody
          tr
            td experiment
            td As long as the run is not yet #[em approved].
          tr
            td approved
            td
              | Can be set to #[tt "1"], can not be set back to #[tt "0"],
              | #[em experiment] #[bcp14 MUST] be set (may be set in the same command).

    section(anchor="MERGER_Current_Files")
      name MERGER/Current/Files service

      t
        | This service is a dataset Service as described in
        | #[xref(target="dim-xml-extension-01") DIM XML Extension],
        | it provides live information about the latest run (ongoing or stopped).

      t
        | To retrieve details about a past run the associated
        | #[xref(target="MERGER_Files_RPC") RPC service]
        | #[bcp14 MUST] be used.

      t
        | #[strong Note:] unless a file is #[em COPIED] or #[em MERGED]
        | its size is an estimation an #[bcp14 MAY] not be accurate.

      figure
        name MERGER/Current/Files Service #[xref(target="RELAX_NG") relax-ng compact schema].
        sourcecode(type='rnc' src='ntof-software-architecture/files/RawFileMerger_Current_Files.rnc')

      section(anchor="MERGER_Files_RPC")
        name MERGER/Files RPC

        t
          | This service is an XML/RPC service as described in
          | #[xref(target='dim-xml-extension-01') DIM XML Extension], it
          | provides information about files associated with a given run.

        figure
          name MERGER/Files/RpcIn
          sourcecode(type='rnc' src='ntof-software-architecture/files/RawFileMerger_Files_RpcIn.rnc')

        t
          | The #[tt MERGER/Files/RpcOut] service will return XML document
          | that matches the #[tt MERGER/Files] service schema, with two
          | specificities:
        ul
          li
            | The root is an #[tt acknowledge] element instead of the
            | #[tt dataset], as described in #[xref(target='dim-xml-extension-01') DIM XML Extension].
          li
            | The #[tt rate] data is omitted.
