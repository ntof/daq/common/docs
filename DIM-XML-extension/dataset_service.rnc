

start = dataset

dataset = element dataset {
  (intData | floatData | stringData | enumData | childData) *
}

# Attributes common to all data types
baseData = (
  attribute name { text },
  attribute index { xsd:unsignedInt } ?,
  attribute unit { text } ?
)

intData = element data { baseData,
  # values 1:int32_t 4:int64_t 6:int8_t 7:int16_t 9:bool
  attribute type { "1"|"4"|"6"|"7"|"9" },
  attribute value { xsd:integer }
}

floatData = element data { baseData,
  # values 2:double 8:float
  # supports complete xsd:double spec, may have an exponent 'e'|'E'
  attribute type { "2"|"8" },
  attribute value { xsd:double }
}

stringData = element data { baseData,
  attribute type { "3" },
  attribute value { text }
}

enumData = element data { baseData,
  attribute type { "5" },
  attribute value { xsd:int },
  attribute valueName { text },
  element enum {
    element value {
      attribute value { xsd:int },
      attribute name { text }
    } +
  }
}

childData = element data { baseData,
  (intData | floatData | stringData | enumData | childData) *
}
