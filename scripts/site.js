

var app = Vue.createApp({
  data: function() { return { docs: null, filters: new Set(), notify: 0 } },
  computed: {
    categories: function() {
      return _.transform(this.docs, function(ret, doc) {
        ret[doc.category] = doc.color;
      }, {});
    },
    filtered: function() {
      if (_.isEmpty(this.filters)) {
        return this.docs;
      }
      else {
        var filters = this.filters;
        return _.filter(this.docs, function(doc) {
          return filters.has(doc.category);
        });
      }
    }
  },
  methods: {
    upperCase: _.upperCase,
    isFilter: function(name) { return this.filters && this.filters.has(name); },
    filterStyle: function(name, color) {
      if (this.isFilter(name)) {
        return { 'background-color': color, 'border-color': color };
      }
    },
    toggleFilter: function(name) {
      var filters = this.filters;
      this.filters = null;
      if (!filters.delete(name)) {
        filters.add(name);
      }
      this.filters = filters; /* force reactivity */
    }
  },
  mounted: function() {
    var self = this;
    $.ajax({ url: 'index.json', cache: false })
    .then(
      function(data) {
        self.docs = _.orderBy(_.map(data, function(value, key) {
          value.name = key; return value;
        }), [ 'name' ], [ 'asc' ]);
      },
      function(error) {
        console.log(error);
      });
  }
});

app.component('Document', {
  name: 'Document',
  template: '<div class="expander"> \
    <div class="expander-trigger doc" @click="open=!open" :class="{ active: open, beforeBorder: !open, draft: doc.draft }"> \
      <div class="doc-tag shadow float-right" :style="{ \'background-color\': doc.color }">{{ upperCase(doc.category) }}</div> \
      <template v-if="doc.draft"> \
        <div class="doc-tag shadow expired float-right mr-1" v-if="expired">EXPIRED</div> \
        <div class="doc-tag shadow draft float-right mr-1" v-else>DRAFT</div> \
      </template> \
      {{ doc.title }} \
    </div> \
    <transition :name="animation"> \
      <div class="expander-body" v-show="open"> \
        <div class="row"> \
          <div class="text-muted doc-info-hdr col-12 col-md-2">Date:</div><div class="col-12 col-md-10">{{ formatDate(doc.date) }}</div> \
          <div class="text-muted doc-info-hdr col-12 col-md-2" v-if="doc.draft">Expires:</div><div class="col-12 col-md-10" v-if="doc.draft">{{ formatDate(doc.expiry) }}</div> \
          <div class="text-muted doc-info-hdr col-12 col-md-2" v-if="!doc.draft">EDMS:</div><div class="col-12 col-md-10" v-if="!doc.draft">{{ doc.number }}</div> \
          <div class="text-muted doc-info-hdr col-12 col-md-2">Abstract:</div><div class="col-12 col-md-10"><pre>{{ doc.abstract }}</pre></div> \
        </div> \
        <div class="row justify-content-center justify-content-md-end"> \
          <a :href="getUrl(doc, \'txt\')" class="m-1 btn btn-sm btn-secondary">TXT</a> \
          <a :href="getUrl(doc, \'pdf\')" class="m-1 btn btn-sm btn-secondary">PDF</a> \
          <a :href="getUrl(doc, \'html\')" class="m-1 btn btn-sm btn-primary">HTML</a> \
        </div> \
      </div> \
    </transition> \
  </div>',
  props: {
    doc: Object,
    animation: { type: String, default: 'rightToLeft' }
  },
  computed: {
    expired: function() {
      return _.isNil(this.doc, 'expiry') ? false : (this.doc.expiry < (_.now() / 1000));
    }
  },
  methods: {
    upperCase: _.upperCase,
    getUrl: function(doc, ext) {
      var path;
      if (_.get(doc, 'draft', true)) {
        path = 'drafts/' + ((_.get(doc, 'expiry') > (_.now() / 1000)) ? 'current/' : 'expired/');
      }
      else {
        path = _.get(doc, 'category') + '/';
      }
      return path + _.get(doc, 'name') + '.' + ext;
    },
    formatDate: function(d) {
      return new Date(d * 1000).toDateString();
    }
  },
  data: function() { return { open: false } }
});


app.mount('#app');

